# paramaterised derivation with dependencies injected (callPackage style)
 { pkgs, stdenv, src ? ../., opam2nix,
   ocaml ? "ocamlPackages_latest.ocaml", plugins ? { },
   plugin_extend ? self: super: { }
 }:

let old_pkgs = pkgs; in

let frama_clang_build =
  { pkgs?old_pkgs,
    stdenv?pkgs.stdenv,
    llvm_version,
    llvm?pkgs.${"llvm_"+llvm_version},
    llvm_package?pkgs.${"llvmPackages_"+llvm_version} } :
(plugins.helpers.simple_plugin
   { inherit pkgs stdenv src opam2nix ocaml plugins plugin_extend;
     name = "frama-clang-on-llvm-" + llvm_version;
     deps = [ llvm_package.clang-unwrapped llvm pkgs.gnused ];
     opamPackages = [ "camlp5=7.14" ];
     preFramaCTests = ''
       echo CONFIGURING Why3 for Frama_Clang.
       export HOME=$(mktemp -d)
       why3 config detect
     '';
   });
    pkgs = import (
      builtins.fetchTarball {
        url = "https://github.com/NixOS/nixpkgs/archive/ed2c99e65f4f5f4bf3bb3a3422f07fc8ec9a97ce.tar.gz";
        sha256="1bp4fkswpl7s2clxbdbff8j42dsns4ihvc5l9399v9kapxb7wx5f";
        }
      )
    {};
in
(frama_clang_build { llvm_version="9"; })
  .extend(
    self: super:
    { on-llvm10 = (frama_clang_build { llvm_version="10"; });
      on-llvm11 = (frama_clang_build { llvm_version="11"; });
      on-llvm13 = (frama_clang_build { inherit pkgs; llvm_version="13"; });
    })
