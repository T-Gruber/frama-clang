/* run.config
 OPT: @MACHDEP@ @CXX@ -print
 DONTRUN: Not yet fully working
 */
#include <bitset>
#include <cassert>
#include <cerrno>
#include <climits>
#include <clocale>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdint>
#include <cstring>
#include <ctime>
#include <cwchar>
#include <exception>
#include <ios>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <locale>
#include <memory>
#include <ostream>
#include <stdexcepts>
#include <streambuf>
#include <string>
#include <system_error>
#include <type_traits>
